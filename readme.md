# sk0r
![sk0r logo](its-a-js-project-so-it-has-to-have-a-shitty-logo.png)

a script to get live scores from hltv.org

## requirements 
- node.js 
- npm
- `http` node package
- nightmare
- electron
- libgtk-2-0
- libgconf-2-4
- xvfb (if you're running it headless)

## setup
- `sudo apt install libgtk2.0-0 libgconf-2-4` (if you're on debian, other
  distros idk)
- `sudo apt install xvfb` (again, on debian)
- `git clone https://tildegit.org/sose/sk0r`
- `cd sk0r`
- `npm install nightmare`
- `npm install http`
- `sudo npm install -g electron --unsafe-perm=true`
- `node sk0r.js`
- or `xvfb-run node sk0r.js` for headless
- the server will serve live game data in JSON format on localhost port 8000
    - not too sure about node's http server security, you might want to
      use a reverse proxy if you're exposing it to the web
- if you don't want to manually refresh the browser page there is the
  `refresh.sh` script in this repo that will do it for you
    - scores are automatically updated, but this is necessary to get new
      matches

## using
- use you favourite http client or web browser and point it to
  'http://localhost:8000'
    - JSON will be served
- scores will update each time you access the page
- to refresh the electron browser page, access 'http://localhost:8000?reload'
    - this will fetch the newest matches, but is generally only necessary if
      they havent been refreshed in a while
    - it also sends a request to hltv's servers, so use it sparingly

## config
- if you want to change the host or port you can change the variables at the
  top of the script
- to modify rate limiting behavior, you can also modify those variables at the
  top of the script as well
    - the rate limit is for the script, and prevents too much refreshing of the
      browser page

## why?
- its really hard to scrape hltv live scores any other way

## notes
- its electron, don't run it on your 340e
- the rate limit functionality is not intuitive, but it was easy to write
- it's named after [this kid](https://www.hltv.org/player/18638/sk0r) (hes good)

## additional notes
- i hate javascript
- i dont know how to write javascript
- i dont know how to use nightmare

## more notes
- hltv doesn't like you scraping them, they'll probably break this and/or send
  me a c&d soon
- in keeping with the theme of naming projects after counter-strike players,
  the next project i release will be a mobile phone operating system named
"ANDROID"

(c) oneseveneight/sose
