const http = require("http");
const Nightmare = require('nightmare')
const nightmare = Nightmare({
    show: false,
})

const hltv_url = "https://hltv.org";
const host = 'localhost';
const port = 8000;

// note that rate limit only applies to reload requests, since they affect hltv's servers
const rate_limit_max_requests = 20; // maximum number of requests allow in the interval
const rate_limit_timer_dec_interval = 5000; // the lower the number the more aggressive the rate limit

function update_scores(hltv_url) {
    var match_rows = document.querySelectorAll("div.teamrows");
    var matches = [];
    for (var i = 0; i < match_rows.length; i++) {
        var match_row = match_rows[i];
        var team_rows = match_row.children;
        var parent = match_row.parentElement;
        var grandparent = match_row.parentElement.parentElement;
        var sibling = match_row.nextElementSibling;
        // if (grandparent.className === "col-box a-reset") { continue; }

        // since the VOD boxes are always at the end we can just break here
        if (grandparent.className === "col-box a-reset") {
            break;
        }

        var event_name;
        var start_time;
        var link = "";
        var stars;
        var lan;
        var team_names = [];
        var team_countries = [];
        var rounds_won = [];
        var maps_won = [];


        for (var j = 0; j < team_rows.length; j++) {
            var team_row = team_rows[j];
            if (team_row.className.includes("teamrow")) {
                var flag_el = team_row.getElementsByClassName("flag")[0];
                var team_el = team_row.getElementsByClassName("team")[0];
                if (typeof flag_el != 'undefined') {
                    team_countries.push(flag_el.getAttribute("title"));
                }
                team_names.push(team_el.innerText);
            }
        }

        if (grandparent.className.includes("hotmatch-box")) {
            stars = Number(parent.getAttribute("stars"));
            lan = Boolean(parent.getAttribute("lan"));
            link = grandparent.getAttribute("href");
            event_name = grandparent.getAttribute("title")
            start_time = "LIVE"
        } else if (grandparent.className.includes("col-box-con result-box")) {
            stars = Number(grandparent.getAttribute("stars"));
            lan = Boolean(grandparent.getAttribute("lan"));
            link = grandparent.getAttribute("href");
            event_name = parent.previousElementSibling.getAttribute("title");
            start_time = "OVER"
        }

        if (sibling.className === "twoRowExtra") {
            score_rows = sibling.children;
            for (var j = 0; j < score_rows.length; j++) {
                var score_row = score_rows[j];
                if (score_row.className === "livescore twoRowExtraRow") {
                    var score_el = score_row.querySelector("[data-livescore-current-map-score='']");
                    var maps_won_el = score_row.querySelector("[data-livescore-maps-won-for='']");
                    if (score_el !== null) {
                        rounds_won.push(score_el.innerText);
                    }
                    if (maps_won_el !== null) {
                        maps_won.push(maps_won_el.innerText);
                    }
                } else if (score_row.className.includes("twoRowExtraRow won") ||
                    score_row.className.includes("twoRowExtraRow lost")) {
                    maps_won.push(score_row.innerText);
                }
            }
        } else if (sibling.className === "middleExtra") {
            start_time = sibling.getAttribute("data-unix");
        }

        if (link !== null) {
            link = hltv_url + link;
        }

        matches.push({
            "event_name": event_name,
            "start_time": start_time,
            "link": link,
            "stars": stars,
            "lan": lan,
            "team_names": team_names,
            "team_countries": team_countries,
            "rounds_won": rounds_won,
            "maps_won": maps_won
        });

    }
    var current_time = new Date().getTime();
    matches.unshift({
        "updated_on": current_time
    });
    return JSON.stringify(matches);
}


async function run(hltv_url, host, port) {
    var rate_limit_timer = 0;
    var on_cooldown = false;
    setInterval(() => {
        if (rate_limit_timer > 0) {
            rate_limit_timer--;
        } else {
            on_cooldown = false;
        }
    }, rate_limit_timer_dec_interval);

    const hltv = nightmare
        .goto(hltv_url)
        .wait("div[class='teamrows']")
        .wait(5000);

    const request_handler = async function(request, response) {
        response.writeHead(200, {
            "Content-Type": "application/json; charset=UTF-8"
        });
        const query_string = request.url.split("?").slice(-1)[0];
        if (query_string === "reload") {
            if (rate_limit_timer >= rate_limit_max_requests) {
                on_cooldown = true;
            }
            if (on_cooldown === false) {
                await hltv.refresh();
                rate_limit_timer++;
            }
        }
        await hltv.evaluate(update_scores, hltv_url)
            .then((matches) => {
                response.write(matches);
                response.end();
            })
            .catch(error => {
                response.write(String(error));
                response.end();
            });
    }

    const server = http.createServer(request_handler);
    server.listen(port, host, () => {
        console.log(`Server is running on ${host}:${port}`);
    });
}

run(hltv_url, host, port);
