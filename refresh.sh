#!/bin/sh
# this script reloads the electron page every $interval to make sure all the
# matches are fresh

interval=300 # 5 minutes

while true
do
    curl -s "https://illegaldrugs.net/skor?reload" >/dev/null
    echo "reloaded on $(date)"
    sleep $interval
done
